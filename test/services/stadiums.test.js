const assert = require('assert');
const app = require('../../src/app');

describe('\'stadiums\' service', () => {
  it('registered the service', () => {
    const service = app.service('stadiums');

    assert.ok(service, 'Registered the service');
  });
});
