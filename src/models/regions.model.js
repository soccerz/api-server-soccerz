const { regionTypes } = require("./_enum-types");
const slugify = require("slugify");

module.exports = function(app) {
  const mongooseClient = app.get("mongooseClient");
  const { Schema } = mongooseClient;
  const ObjectId = Schema.Types.ObjectId;
  const regions = new Schema(
    {
      title: { type: String, required: true, unique: true },
      slug: {
        type: String,
        default: function() {
          return slugify(this.title).toLowerCase();
        }
      },
      type: {
        type: String,
        enum: regionTypes
      },
      inRegion: {
        type: ObjectId,
        ref: "regions"
      }
    },
    {
      timestamps: true
    }
  );

  return mongooseClient.model("regions", regions);
};
