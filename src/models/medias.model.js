const { mediaTypes } = require("./_enum-types");
module.exports = function(app) {
  const mongooseClient = app.get("mongooseClient");
  const { Schema } = mongooseClient;
  const medias = new Schema(
    {
      title: { type: String, required: true },
      type: { type: String, enum: mediaTypes },
      alt: { type: String },
      url: { type: String, required: true, unique: true },
      caption: { type: String },
      specifications: {
        kiloBytes: { type: Number },
        mimeType: { type: String },
        quality: { type: String },
        dimensions: { height: { type: Number }, width: { type: Number } },
        durations: {
          hours: { type: Number },
          minutes: { type: Number },
          seconds: { type: Number }
        }
      }
    },
    { timestamps: true }
  );

  return mongooseClient.model("medias", medias);
};
