const { teamTypes } = require("./_enum-types");

module.exports = function(app) {
  const mongooseClient = app.get("mongooseClient");
  const { Schema } = mongooseClient;
  const ObjectId = Schema.Types.ObjectId;
  const teams = new Schema(
    {
      title: { type: String, required: true, unique: true },
      shortTitle: { type: String },
      type: { type: String, enum: teamTypes, default: "football-club" },
      fullTitle: { type: String },
      aliasTitle: { type: String },
      emblemUrl: { type: String },
      featuredImage: { type: ObjectId, ref: "medias" },
      ground: { type: ObjectId, ref: "stadiums" },
      association: { type: ObjectId, ref: "associations" },
      country: { type: ObjectId, ref: "regions" },
      memberOfAssociation: { type: ObjectId, ref: "associations" },
      manager: { type: ObjectId, ref: "persons" },
      squads: [{ type: ObjectId, ref: "persons" }],
      description: { type: String },
      leagues: [{ type: ObjectId, ref: "leagues" }],
      matches: [{ type: ObjectId, ref: "matches" }],
      referenceUrls: [
        {
          provider: { type: String },
          title: { type: String },
          url: { type: String }
        }
      ]
    },
    { timestamps: true }
  );

  return mongooseClient.model("teams", teams);
};
