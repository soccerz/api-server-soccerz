// persons-model.js - A mongoose model
//
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.
module.exports = function(app) {
  const mongooseClient = app.get("mongooseClient");
  const { Schema } = mongooseClient;
  const ObjectId = Schema.Types.ObjectId;
  const persons = new Schema(
    {
      name: { type: String, required: true, unique: true },
      dateOfBirth: { type: Date },
      status: {
        type: String,
        enum: ["active", "retired", "non-active", "deceased"]
      },
      nationality: {
        type: ObjectId,
        ref: "regions"
      },
      featuredImage: {
        type: ObjectId,
        ref: "medias"
      },
      knownAs: [{ type: String }],
      timeline: [
        {
          year: {
            start: { type: Number },
            end: { type: Number }
          },
          related: {
            model: { type: String, enum: ["teams", "associations"] },
            entity: { type: ObjectId, refPath: "related.model" }
          },
          current: { type: Boolean, default: false }
        }
      ],
      description: { type: String },
      referenceUrls: [
        {
          provider: { type: String },
          title: { type: String },
          url: { type: String }
        }
      ]
    },
    {
      timestamps: true
    }
  );

  return mongooseClient.model("persons", persons);
};
