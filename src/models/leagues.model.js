const { teamTypes, tableMemberTypes } = require("./_enum-types");

module.exports = function(app) {
  const mongooseClient = app.get("mongooseClient");
  const { Schema } = mongooseClient;
  const ObjectId = Schema.Types.ObjectId;
  const leagues = new Schema(
    {
      title: { type: String, required: true },
      shortTitle: { type: String },
      slug: { type: String, unique: true },
      emblemUrl: { type: String },
      parentLeague: { type: ObjectId, ref: "leagues" },
      isConcluded: { type: Boolean, default: false },
      scopedRegion: { type: ObjectId, ref: "regions" },
      hostRegions: [{ type: ObjectId, ref: "regions" }],
      association: { type: ObjectId, ref: "associations" },
      dates: {
        start: { type: Date },
        end: { type: Date }
      },
      phases: [{ type: String }],
      teams: {
        type: {
          type: String,
          enum: teamTypes
        },
        count: { type: Number },
        list: [{ type: ObjectId, ref: "teams" }]
      },
      matches: [{ type: ObjectId, ref: "matches" }],
      venues: [{ type: ObjectId, ref: "stadiums" }],
      descriptions: { type: String },
      results: [{ tag: { type: String }, content: { type: String } }],
      tables: [
        {
          title: { type: String },
          membersType: {
            type: String,
            enum: tableMemberTypes
          },
          membersCount: { type: Number },
          tableData: [
            {
              position: { type: Number },
              entity: {
                type: ObjectId,
                refPath: "membersType"
              },
              fields: [
                {
                  tag: { type: String }
                },
                {
                  counts: { type: Number }
                }
              ]
            }
          ]
        }
      ]
    },
    {
      timestamps: true
    }
  );

  return mongooseClient.model("leagues", leagues);
};
