const matchEventTypes = [
  "yellow-card",
  "second-yellow-card",
  "red-card",
  "penalty-kick",
  "penalty-saved",
  "clearance",
  "free-kick",
  "off-side",
  "corner-kick",
  "goal",
  "saved",
  "shot-on-post",
  "injured",
  "subtitution"
];
const matchStatusTypes = ["scheduled", "live", "concluded", "suspended"];
const matchResultTypes = ["home-win", "away-win", "draw", "not-available"];
const regionTypes = ["country", "sub-confederation", "confederation", "world"];
const teamTypes = ["national-team", "football-club", "other"];
const tableMemberTypes = ["persons", "teams"];
const mediaTypes = ["video", "photo"];
const stadiumStatusTypes = [
  "operational",
  "under-renovation",
  "under-construction",
  "demolished",
  "inactive"
];

module.exports = {
  matchEventTypes,
  matchResultTypes,
  matchStatusTypes,
  regionTypes,
  teamTypes,
  tableMemberTypes,
  mediaTypes,
  stadiumStatusTypes
};
