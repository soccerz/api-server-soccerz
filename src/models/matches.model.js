const {
  matchEventTypes,
  matchResultTypes,
  matchStatusTypes
} = require("./_enum-types");

module.exports = function(app) {
  const mongooseClient = app.get("mongooseClient");
  const { Schema } = mongooseClient;
  const ObjectId = Schema.Types.ObjectId;
  const matches = new Schema(
    {
      dateTime: { type: Date, required: true },
      stadium: { type: ObjectId, ref: "stadiums" },
      league: { type: ObjectId, ref: "leagues" },
      leagueAdditonalInfo: { type: String },
      status: {
        type: String,
        enum: matchStatusTypes
      },
      result: {
        type: String,
        enum: matchResultTypes,
        default: "not-available"
      },
      home: {
        team: { type: ObjectId, ref: "teams" },
        starters: [{ type: ObjectId, ref: "persons" }],
        goal: {
          ht: { type: Number },
          ft: { type: Number },
          final: { type: Number },
          scorer: [
            {
              minute: { type: Number },
              player: { type: ObjectId, ref: "persons" },
              remark: { type: String }
            }
          ]
        },
        timeline: [
          {
            minute: { type: Number },
            remarks: { type: String },
            type: {
              type: String,
              enum: matchEventTypes
            }
          }
        ]
      },
      away: {
        team: { type: ObjectId, ref: "teams" },
        starters: [{ type: ObjectId, ref: "persons" }],
        goal: {
          ht: { type: Number },
          ft: { type: Number },
          final: { type: Number },
          scorer: [
            {
              minute: { type: Number },
              player: { type: ObjectId, ref: "persons" },
              remark: { type: String }
            }
          ]
        },
        timeline: [
          {
            minute: { type: Number },
            remarks: { type: String },
            type: {
              type: String,
              enum: matchEventTypes
            }
          }
        ]
      },
      medias: [{ type: ObjectId, ref: "medias" }]
    },
    { timestamps: true }
  );

  return mongooseClient.model("matches", matches);
};
