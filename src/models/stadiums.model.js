const { stadiumStatusTypes } = require("./_enum-types");

module.exports = function(app) {
  const mongooseClient = app.get("mongooseClient");
  const { Schema } = mongooseClient;
  const ObjectId = Schema.Types.ObjectId;
  const stadiums = new Schema(
    {
      title: { type: String, required: true, unique: true },
      location: { type: String },
      inMap: {
        lat: { type: Number },
        long: { type: Number }
      },
      status: {
        type: String,
        enum: stadiumStatusTypes,
        default: "operational"
      },
      featuredImage: {
        type: ObjectId,
        ref: "medias"
      },
      capacity: { type: Number },
      category: { type: String },
      tenants: [{ type: ObjectId, ref: "teams" }],
      countryRegion: { type: ObjectId, ref: "regions" },
      timelines: [
        {
          year: { type: Number },
          text: { type: String }
        }
      ],
      referenceUrls: [
        {
          provider: { type: String },
          title: { type: String },
          url: { type: String }
        }
      ],
      leagues: [
        {
          type: ObjectId,
          ref: "leagues"
        }
      ],
      matches: [
        {
          type: ObjectId,
          ref: "matches"
        }
      ]
    },
    {
      timestamps: true
    }
  );

  return mongooseClient.model("stadiums", stadiums);
};
