// associations-model.js - A mongoose model
//
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.
module.exports = function(app) {
  const mongooseClient = app.get("mongooseClient");
  const { Schema } = mongooseClient;
  const ObjectId = Schema.Types.ObjectId;
  const associations = new Schema(
    {
      title: { type: String, required: true, unique: true },
      shortTitle: { type: String },
      scopeRegion: { type: ObjectId, ref: "regions" },
      emblemUrl: { type: String },
      headQuarter: { type: String },
      referenceUrls: [
        {
          provider: { type: String },
          title: { type: String },
          url: { type: String }
        }
      ],
      leadership: [
        {
          period: { start: { type: Number }, end: { type: Number } },
          person: { type: ObjectId, ref: "persons" },
          position: { type: String }
        }
      ],
      description: { type: String },
      leagues: [{ type: ObjectId, ref: "leagues" }]
    },
    { timestamps: true }
  );

  return mongooseClient.model("associations", associations);
};
