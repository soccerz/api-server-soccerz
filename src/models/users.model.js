// users-model.js - A mongoose model
//
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.
module.exports = function(app) {
  const mongooseClient = app.get("mongooseClient");
  const users = new mongooseClient.Schema(
    {
      username: { type: String, unique: true, lowercase: true },
      email: { type: String, unique: true, lowercase: true },
      password: { type: String },
      profiles: [
        {
          provider: { type: String },
          identifier: { type: String },
          url: { type: String }
        }
      ],
      verification: {
        token: { type: String },
        expired: { type: Date }
      },
      isActive: { type: Boolean, default: false },
      isVerified: { type: Boolean, default: false }
    },
    {
      timestamps: true
    }
  );

  return mongooseClient.model("users", users);
};
