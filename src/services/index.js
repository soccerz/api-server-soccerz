const users = require('./users/users.service.js');
const regions = require('./regions/regions.service.js');
const associations = require('./associations/associations.service.js');
const leagues = require('./leagues/leagues.service.js');
const medias = require('./medias/medias.service.js');
const persons = require('./persons/persons.service.js');
const stadiums = require('./stadiums/stadiums.service.js');
const matches = require('./matches/matches.service.js');
const teams = require('./teams/teams.service.js');
// eslint-disable-next-line no-unused-vars
module.exports = function (app) {
  app.configure(users);
  app.configure(regions);
  app.configure(associations);
  app.configure(leagues);
  app.configure(medias);
  app.configure(persons);
  app.configure(stadiums);
  app.configure(matches);
  app.configure(teams);
};
