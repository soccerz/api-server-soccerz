// Initializes the `stadiums` service on path `/stadiums`
const createService = require('feathers-mongoose');
const createModel = require('../../models/stadiums.model');
const hooks = require('./stadiums.hooks');

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/stadiums', createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service('stadiums');

  service.hooks(hooks);
};
